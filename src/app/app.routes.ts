import { RouterModule, Routes } from "@angular/router";
import { InicioComponent } from "./components/inicio/inicio.component";
import { ColumnaComponent } from "./components/columna/columna.component";
import { FinComponent } from "./components/fin/fin.component";


import { Lateral1Component } from "./components/lateral1/lateral1.component";
import { Lateral2Component } from "./components/lateral2/lateral2.component";
import { Lateral3Component } from "./components/lateral3/lateral3.component";
import { Lateral4Component } from "./components/lateral4/lateral4.component";
import { Lateral5Component } from "./components/lateral5/lateral5.component";
const Routes1: Routes =[
    {path: 'inicio', component: InicioComponent},
    {path: 'columna', component: ColumnaComponent},
    {path: 'fin', component:FinComponent},
    {path: 'lateral1', component:Lateral1Component},
    {path: 'lateral2', component:Lateral2Component},
    {path: 'lateral3', component:Lateral3Component},
    {path: 'lateral4', component:Lateral4Component},
    {path: 'lateral5', component:Lateral5Component},
    {path: '**', pathMatch:'full', redirectTo:'inicio'}
]



export const Routing= RouterModule.forRoot(Routes1)
