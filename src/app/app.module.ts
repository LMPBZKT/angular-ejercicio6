import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NadbarComponent } from './components/nadbar/nadbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { ColumnaComponent } from './components/columna/columna.component';
import { FinComponent } from './components/fin/fin.component';


import {Routing}from"./app.routes";
import { Lateral1Component } from './components/lateral1/lateral1.component';
import { Lateral2Component } from './components/lateral2/lateral2.component';
import { Lateral3Component } from './components/lateral3/lateral3.component';
import { Lateral4Component } from './components/lateral4/lateral4.component';
import { Lateral5Component } from './components/lateral5/lateral5.component'


@NgModule({
  declarations: [
    AppComponent,
    NadbarComponent,
    SidebarComponent,
    InicioComponent,
    ColumnaComponent,
    FinComponent,
    Lateral1Component,
    Lateral2Component,
    Lateral3Component,
    Lateral4Component,
    Lateral5Component
  ],
  imports: [
    BrowserModule,
    Routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
