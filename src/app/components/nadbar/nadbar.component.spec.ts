import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NadbarComponent } from './nadbar.component';

describe('NadbarComponent', () => {
  let component: NadbarComponent;
  let fixture: ComponentFixture<NadbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NadbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NadbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
