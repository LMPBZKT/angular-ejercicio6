import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lateral5Component } from './lateral5.component';

describe('Lateral5Component', () => {
  let component: Lateral5Component;
  let fixture: ComponentFixture<Lateral5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lateral5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lateral5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
